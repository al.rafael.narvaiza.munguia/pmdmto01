package com.example.numerosprimos

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val inputNumber = findViewById<EditText>(R.id.inputNumber)
        val calculateButton = findViewById<Button>(R.id.calculateButton)

        calculateButton.setOnClickListener {
            if (calculateButton.text.toString() == "Limpiar") {
                inputNumber.setText("")
                inputNumber.requestFocus()
                calculateButton.text = "Calcular"
            } else {
                if (inputNumber.text.isEmpty()) {
                    val warning = "Introduzca un número"
                    Toast.makeText(this, warning, Toast.LENGTH_SHORT).show()
                } else {
                    val number = inputNumber.text.toString().toLong()
                    val message = esPrimo(number)
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
                    //displayResult.text = message
                }
                calculateButton.text="Limpiar"
            }
        }
    }

    fun esPrimo(numero: Long): String? {
        var esPrimo = true
        val message: String
        var i = 2
        while (i < Math.sqrt(numero.toDouble()) && esPrimo) {
            if (numero % i == 0L) {
                esPrimo = false
            }
            i++
        }
        message = if (esPrimo) {
            " es primo"
        } else {
            " no es primo"
        }
        return "el número $numero$message"
    }
}


