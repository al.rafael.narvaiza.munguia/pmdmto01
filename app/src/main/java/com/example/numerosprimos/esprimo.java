package com.example.numerosprimos;

import java.util.Scanner;

public class esprimo {

    public String esPrimo(long numero){

        boolean esPrimo = true;
        String message;
        for (int i = 2; i < Math.sqrt(numero) && esPrimo; i ++) {
            if ((numero % i) == 0) {
                esPrimo = false;
            }
        }
        if (esPrimo) {
            message = " es primo";
        } else {
            message = " no es primo";
        }
        return "el número " + numero + message;
    }
}
