Develop an aplication under SDK27 which contains one image, one text view, one button and one label.
User must type a number on text view and when the button will be pressed software must check if the given number is prime or not.
Developer must catch all the exceptions and if there is any exception, a message must be shown. In any case the execute of the aplication would be stopped.
